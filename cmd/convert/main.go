package main

import (
	"errors"
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/thorchain/tss/go-tss/conversion"
)

func main() {
	conversion.SetupBech32Prefix()
	app := &cli.App{
		Name:  "peer",
		Usage: "get peer id from thor node public key",
		Commands: []cli.Command{
			{
				Name:        "peer",
				Usage:       "convert the given thornode pub key to peer id",
				UsageText:   "",
				Description: "convert the given thornode pubkey key to peer id",
				ArgsUsage:   "",
				Action:      getPeerID,
				Flags: []cli.Flag{
					cli.StringFlag{
						Name:     "pubkey",
						Usage:    "thornode public key",
						Required: true,
						Hidden:   false,
					},
				},
			},
			{
				Name:        "pubkey",
				Usage:       "convert the given peer id to thornode bech32 public key",
				UsageText:   "",
				Description: "convert the given peer id to thornode bech32 public key",
				Action:      getPubKey,
				Flags: []cli.Flag{
					cli.StringFlag{
						Name:     "peerid",
						Usage:    "p2p peer id",
						Required: true,
						Hidden:   false,
					},
				},
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func getPeerID(ctx *cli.Context) error {
	pubKey := ctx.String("pubkey")
	if len(pubKey) == 0 {
		return errors.New("thornode pubkey can't be empty")
	}
	pID, err := conversion.GetPeerIDFromPubKey(pubKey)
	if err != nil {
		return fmt.Errorf("fail to get peer id from thornode pubkey")
	}
	fmt.Println("peer id:", pID.String())
	return nil
}

func getPubKey(ctx *cli.Context) error {
	peerID := ctx.String("peerid")
	if len(peerID) == 0 {
		return errors.New("peer id is not present")
	}
	pkey, err := conversion.GetPubKeyFromPeerID(peerID)
	if err != nil {
		return fmt.Errorf("fail to get pubkey from peer id: %w", err)
	}
	fmt.Println("thornode pubkey is: ", pkey)
	return nil
}
