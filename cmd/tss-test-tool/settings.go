package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

// ConfigItem for tss test tool
type ConfigItem struct {
	TSSUrl string `json:"tss_url"`
	PubKey string `json:"pub_key"`
}

// Config for tss test tool
type Config struct {
	Nodes []ConfigItem `json:"nodes"`
}

func ReadFromJson(fileName string) (*Config, error) {
	buf, err := ioutil.ReadFile(fileName)
	if nil != err {
		return nil, fmt.Errorf("fail to read file(%s): %w", fileName, err)
	}
	var cfg Config
	if err := json.Unmarshal(buf, &cfg); nil != err {
		return nil, fmt.Errorf("fail to unmarshal config:%w", err)
	}
	return &cfg, nil
}
