package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"sort"
	"sync"
	"sync/atomic"
	"syscall"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/urfave/cli"
	keygen2 "gitlab.com/thorchain/tss/go-tss/keygen"
	keysign2 "gitlab.com/thorchain/tss/go-tss/keysign"
)

type MessageType uint8

const (
	KeyGen MessageType = iota
	KeySign
)

var (
	lock             sync.Mutex
	signedMessages   int64
	statLock         sync.Mutex
	stat             = make(map[string]int64)
	startTime        = time.Now()
	parallelCounters []*parallelCounter
)

type parallelCounter struct {
	Index   int
	Total   int64
	PoolKey string
}

func main() {
	app := cli.NewApp()
	app.Name = "TSS test tool"
	app.Usage = "TSS test tool is written to test TSS Keygen and Keysign process, it continuously generate new key and use the new key to sign messages  "
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:     "cfg,c",
			Usage:    "config file",
			EnvVar:   "",
			Required: false,
			Hidden:   false,
			Value:    "config.json",
		},
		cli.IntFlag{
			Name:  "parallel,p",
			Usage: "parallel",
			Value: 1,
		},
	}
	app.Action = appAction
	if err := app.Run(os.Args); err != nil {
		log.Fatal().Err(err).Msg("fail")
	}
}

func appAction(ctx *cli.Context) error {
	configFile := ctx.String("cfg")
	if len(configFile) == 0 {
		return errors.New("please specify your config file")
	}
	p := ctx.Int("parallel")

	cfg, err := ReadFromJson(configFile)
	if nil != err {
		return err
	}
	keys := make([]string, 0, len(cfg.Nodes))
	for _, item := range cfg.Nodes {
		keys = append(keys, item.PubKey)
	}
	stopChan := make(chan struct{})
	wg := &sync.WaitGroup{}
	receivers := make([]chan *commandMessage, len(cfg.Nodes))
	for idx, item := range cfg.Nodes {
		receivers[idx] = make(chan *commandMessage)
		for i := 0; i < p; i++ {
			wg.Add(1)
			go worker(receivers[idx], stopChan, item.TSSUrl, keys, wg, idx)
		}
	}
	parallelCounters = make([]*parallelCounter, p)
	lock.Lock()
	for i := 0; i < p; i++ {
		wg.Add(1)
		pc := &parallelCounter{
			Index:   i,
			Total:   0,
			PoolKey: "",
		}
		parallelCounters[i] = pc
		go commander(receivers, stopChan, keys, wg, pc)
	}
	lock.Unlock()
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGKILL, syscall.SIGTERM)
	<-ch
	close(stopChan)
	wg.Wait()
	// let's wait for signals
	return nil
}

type commandMessage struct {
	MessageType
	Payload []byte
	Result  chan string
	Parties []string
	Index   int
}

func commander(receivers []chan *commandMessage, stopchan <-chan struct{}, keys []string, wg *sync.WaitGroup, pc *parallelCounter) {
	log.Info().Msgf("commander started")
	defer func() {
		log.Info().Msgf("commander stopped")
		wg.Done()
	}()

	for {
		select {
		case <-stopchan:
			return
		case <-time.After(time.Second * 5):
			if len(pc.PoolKey) == 0 {
				log.Info().Msg("start keygen ceremony")
				startKeygen(receivers, stopchan, pc.Index)
				atomic.AddInt64(&pc.Total, 1)
			} else {
				signerParty := getKeys(keys)
				log.Info().Msg("start key sign")
				startKeySign(receivers, stopchan, pc.Total, signerParty, pc.Index)
				atomic.AddInt64(&pc.Total, 1)
			}

		}
	}
}

func startKeySign(receives []chan *commandMessage, stopchan <-chan struct{}, total int64, keys []string, index int) {
	wg := &sync.WaitGroup{}
	for _, item := range receives {
		wg.Add(1)
		time.Sleep(time.Millisecond * 500)
		go sendKeySignCommand(item, stopchan, wg, total, keys, index)
	}
	wg.Wait()
	log.Info().Msg("key sign finished")
	statLock.Lock()
	defer statLock.Unlock()
	success, fail := 0, 0
	for _, item := range stat {
		if item > 0 {
			success++
		} else {
			fail++
		}
	}
	log.Info().Msgf("Successful:%d,Fail:%d,total:%d , duration: %s", success, fail, len(stat), time.Since(startTime))
}
func GetThreshold(value int) int {
	threshold := int(math.Ceil(float64(value)*2.0/3.0)) - 1
	return threshold
}
func getKeys(keys []string) []string {
	total := len(keys)
	threshold := GetThreshold(total) + 1
	var result []string
	sort.SliceStable(keys, func(i, j int) bool {
		return keys[i] < keys[j]
	})
	src := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(src)
	for {
		idx := rnd.Intn(total)
		item := keys[idx]
		found := false
		for _, s := range result {
			if item == s {
				found = true
				break
			}

		}
		if !found {
			result = append(result, item)
		}
		if len(result) == threshold {
			break
		}
	}
	return result
}

func sendKeySignCommand(dest chan<- *commandMessage, stopchan <-chan struct{}, wg *sync.WaitGroup, total int64, keys []string, idx int) {
	defer wg.Done()
	keySignMsg := &commandMessage{
		MessageType: KeySign,
		Payload:     []byte(fmt.Sprintf("helloWorld:%d:%d", total, idx)),
		Result:      make(chan string),
		Parties:     keys,
		Index:       idx,
	}
	select {
	case <-stopchan:
		return
	case dest <- keySignMsg:
		log.Info().Int("parallel", idx).Int64("number-of-messages", total).Msg("start to wait for key sign result..")
		select {
		case <-stopchan:
			return
		case <-keySignMsg.Result:
			return
		}
	}
}

func startKeygen(receivers []chan *commandMessage, stopchan <-chan struct{}, idx int) {
	wg := &sync.WaitGroup{}
	for _, item := range receivers {
		wg.Add(1)
		go sendKeyGenCommand(item, stopchan, wg, idx)
	}

	wg.Wait()
	log.Info().Msg("keygen finished")
	return
}

func sendKeyGenCommand(dest chan<- *commandMessage, stopchan <-chan struct{}, wg *sync.WaitGroup, idx int) {
	defer wg.Done()
	keygenMsg := commandMessage{
		MessageType: KeyGen,
		Payload:     nil,
		Result:      make(chan string),
		Index:       idx,
	}
	select {
	case <-stopchan:
		return
	case dest <- &keygenMsg:
		log.Info().Int("parallel", idx).Msg("start to wait for keygen result..")
		select {
		case <-stopchan:
			return
		case <-keygenMsg.Result:
			return
		}
	}

}

// worker is the one to do the job
func worker(input <-chan *commandMessage, stopchan <-chan struct{}, tssUrl string, keys []string, wg *sync.WaitGroup, idx int) {
	log.Info().Msgf("worker %d started", idx)
	defer func() {
		log.Info().Msgf("work %d finished", idx)
		wg.Done()

	}()
	for {
		select {
		case <-stopchan:
			return
		case msg := <-input:
			switch msg.MessageType {
			case KeyGen:
				if err := keygen(tssUrl, keys, msg.Index); nil != err {
					log.Error().Err(err).Msg("fail to keygen")
				}
			case KeySign:
				if err := keysign(tssUrl, getCurrentPoolPubKey(msg.Index), msg.Payload, msg.Parties); nil != err {
					log.Error().Err(err).Msg("fail to keysign")
				}
			}
			msg.Result <- "finished"
		}
	}
}

func getCurrentPoolPubKey(idx int) string {
	lock.Lock()
	defer lock.Unlock()
	return parallelCounters[idx].PoolKey
}
func setCurrentPoolPubKey(idx int, key string) {
	lock.Lock()
	defer lock.Unlock()
	parallelCounters[idx].PoolKey = key
}

func keysign(u, poolPubKey string, payload []byte, signerKeys []string) error {
	c := http.Client{
		//Timeout: time.Second * 35,
	}
	keySignUrl, err := url.Parse(u)
	if nil != err {
		return fmt.Errorf("fail to parse (%s): %w", u, err)
	}
	keySignUrl.Path = "keysign"
	req := keysign2.Request{
		PoolPubKey:    poolPubKey,
		Message:       base64.StdEncoding.EncodeToString(payload),
		SignerPubKeys: signerKeys,
	}
	buf, err := json.Marshal(req)
	if nil != err {
		return fmt.Errorf("fail to marshal request to json:%w", err)
	}
	resp, err := c.Post(keySignUrl.String(), "application/json", bytes.NewBuffer(buf))
	if nil != err {
		return fmt.Errorf("fail to post keysign request to( %s ) %w", u, err)
	}
	defer func() {
		if err := resp.Body.Close(); nil != err {
			log.Error().Err(err).Msg("fail to close response body")
		}
	}()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("status(%d) unexpected response code", resp.StatusCode)
	}
	result, err := ioutil.ReadAll(resp.Body)
	if nil != err {
		return fmt.Errorf("fail to read response body")
	}
	var tssResp keysign2.Response
	if err := json.Unmarshal(result, &tssResp); nil != err {
		return fmt.Errorf("fail to unmarshal result :%w", err)
	}
	statLock.Lock()
	defer statLock.Unlock()
	if len(tssResp.S) > 0 {
		stat[string(payload)]++
	} else {
		stat[string(payload)]--
	}
	log.Info().Msgf("======> request url:%s, result:%+v", keySignUrl.String(), tssResp)
	return nil
}

func keygen(u string, keys []string, idx int) error {
	c := http.Client{
		Timeout: time.Second * 130,
	}
	keygenUrl, err := url.Parse(u)
	if nil != err {
		return fmt.Errorf("fail to parse (%s): %w", u, err)
	}
	keygenUrl.Path = "keygen"

	req := keygen2.Request{
		Keys: keys,
	}
	buf, err := json.Marshal(req)
	if nil != err {
		return fmt.Errorf("fail to marshal request to json:%w", err)
	}
	resp, err := c.Post(keygenUrl.String(), "application/json", bytes.NewBuffer(buf))
	if nil != err {
		return fmt.Errorf("fail to post keygen request to( %u ) %w", u, err)
	}
	defer func() {
		if err := resp.Body.Close(); nil != err {
			log.Error().Err(err).Msg("fail to close response body")
		}
	}()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("status(%d) unexpected response code", resp.StatusCode)
	}
	result, err := ioutil.ReadAll(resp.Body)
	if nil != err {
		return fmt.Errorf("fail to read response body")
	}
	var tssResp keygen2.Response
	if err := json.Unmarshal(result, &tssResp); nil != err {
		return fmt.Errorf("fail to unmarshal result :%w", err)
	}
	log.Info().Msgf("%+v", tssResp)
	setCurrentPoolPubKey(idx, tssResp.PubKey)

	return nil
}
