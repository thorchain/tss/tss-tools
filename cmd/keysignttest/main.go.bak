package main

import (
	"fmt"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	ctypes "github.com/binance-chain/go-sdk/common/types"
	types2 "gitlab.com/thorchain/thornode/bifrost/thorclient/types"
	"gitlab.com/thorchain/thornode/cmd"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/rs/zerolog/log"
	"github.com/urfave/cli"
	"gitlab.com/thorchain/thornode/bifrost/binance"
	"gitlab.com/thorchain/thornode/bifrost/config"
	"gitlab.com/thorchain/thornode/common"
)

func main() {
	ctypes.Network = ctypes.TestNetwork
	app := cli.NewApp()
	app.Name = "TSS keysign client"
	app.Usage = "TSS keysign client used to simulate how we are going to sign tx and send to binance chain"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "dexhost",
			Usage:  "binance dex host",
			Hidden: false,
			Value:  "testnet-dex.binance.org",
		},
		cli.StringSliceFlag{
			Name:     "tsshosts,ts",
			Usage:    "tss hosts include port",
			Required: true,
			Hidden:   false,
		},
		cli.StringFlag{
			Name:     "to",
			Usage:    "to address",
			Required: false,
			Hidden:   false,
			Value:    "",
		},
		cli.StringFlag{
			Name:     "pubkey",
			Usage:    "pubkey",
			Required: true,
			Hidden:   false,
		},
	}
	app.Action = appAction
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal().Err(err).Msg("failed")
	}

}

func appAction(c *cli.Context) error {
	cfg := sdk.GetConfig()
	cfg.SetBech32PrefixForAccount(cmd.Bech32PrefixAccAddr, cmd.Bech32PrefixAccPub)
	cfg.SetBech32PrefixForValidator(cmd.Bech32PrefixValAddr, cmd.Bech32PrefixValPub)
	cfg.SetBech32PrefixForConsensusNode(cmd.Bech32PrefixConsAddr, cmd.Bech32PrefixConsPub)
	cfg.Seal()
	dexhost := c.String("dexhost")
	tsshosts := c.StringSlice("tsshosts")
	pubKey := c.String("pubkey")
	to := c.String("to")
	wg := sync.WaitGroup{}

	allSource := make([]chan types2.TxArrayItem, 0)
	for idx, item := range tsshosts {
		u, err := url.Parse(item)
		if nil != err {
			return fmt.Errorf("fail to parse %s,%w", item, err)
		}

		p := u.Port()
		port, err := strconv.Atoi(p)
		if nil != err {
			return fmt.Errorf("fail to parse port %s,%w", p, err)
		}
		tssCfg := config.TSSConfiguration{
			Scheme: "http",
			Host:   u.Hostname(),
			Port:   port,
		}
		cfg := config.ChainConfiguration{
			RPCHost:    dexhost,
			PrivateKey: "asfd",
		}

		log.Info().Msgf("config:%v", cfg)
		input := make(chan types2.TxArrayItem)
		wg.Add(1)
		go signAndBroadcast(cfg, tssCfg, idx, input, &wg)
		allSource = append(allSource, input)
		time.Sleep(time.Second * 10)
	}
	// generate 100 tx
	wgGenerator := sync.WaitGroup{}
	wgGenerator.Add(1)
	go txGenerator(&wgGenerator, allSource, to, pubKey)
	wgGenerator.Wait()
	// wait to exist
	for _, item := range allSource {
		close(item)
	}
	wg.Wait()
	return nil
}

func txGenerator(wg *sync.WaitGroup, allSource []chan types2.TxArrayItem, to, pubkey string) {
	defer wg.Done()
	defer log.Info().Msg("finished generateing txout")

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGKILL, syscall.SIGTERM)
	pk, err := common.NewPubKey(pubkey)
	if nil != err {
		log.Error().Err(err).Msg("fail to parse pubkey")
	}
	for i := 0; i < 100; i++ {
		tai := types2.TxArrayItem{
			VaultPubKey: pk,
			SeqNo:       strconv.Itoa(i),
			To:          to,
			Coin:        common.NewCoin(common.BNBAsset, sdk.NewUint(common.One/10)),
			Memo:        fmt.Sprintf("johnny-test-%d", i),
		}

		for _, c := range allSource {
			select {
			case <-ch: // receive cancel signal
				return
			case c <- tai:
			}
		}
		time.Sleep(time.Second * 5)
	}
}

func signAndBroadcast(cfg config.BinanceConfiguration, tsscfg config.TSSConfiguration, idx int, input <-chan types2.TxArrayItem, wg *sync.WaitGroup) {
	defer wg.Done()
	p, err := binance.NewBinance(cfg, true, tsscfg)
	if nil != err {
		log.Error().Err(err).Msg("fail to create binance instance")
	}
	height := int64(1)
	for {
		txOut, more := <-input
		if !more {
			return
		}
		hexBytes, _, err := p.SignTx(txOut, height)
		if nil != err {
			log.Error().Err(err).Int("idx", idx).Msg("fail to sign a tx out")
			continue
		}
		// todo here we might double spending
		err = p.BroadcastTx(hexBytes)
		if nil != err {
			log.Error().Err(err).Int("idx", idx).Msg("fail to broadcast tx to binance")
			continue
		}

		height++
	}

}
