package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/sethgrid/pester"
	"github.com/urfave/cli"
	"gitlab.com/thorchain/thornode/cmd"

	"gitlab.com/thorchain/tss/go-tss/keygen"
)

func main() {
	app := cli.NewApp()
	app.Name = "TSS keygen client"
	app.Usage = "TSS keygen client used to send keygen request to local TSS keygen node"
	app.Flags = []cli.Flag{

		cli.StringFlag{
			Name:     "url,u",
			Usage:    "url of the tss service",
			EnvVar:   "URL",
			Required: true,
			Hidden:   false,
			Value:    "http://127.0.0.1:8080/keygen",
		},
		cli.StringSliceFlag{
			Name:     "pubkey",
			Usage:    "pubkey",
			Required: true,
			Hidden:   false,
		},
	}
	app.Action = appAction
	err := app.Run(os.Args)

	if err != nil {
		log.Fatal(err)
	}
}

func appAction(c *cli.Context) error {
	config := sdk.GetConfig()
	config.SetBech32PrefixForAccount(cmd.Bech32PrefixAccAddr, cmd.Bech32PrefixAccPub)
	config.SetBech32PrefixForValidator(cmd.Bech32PrefixValAddr, cmd.Bech32PrefixValPub)
	config.SetBech32PrefixForConsensusNode(cmd.Bech32PrefixConsAddr, cmd.Bech32PrefixConsPub)
	config.Seal()

	pubkeys := c.StringSlice("pubkey")

	url := c.String("url")
	req := keygen.Request{
		Keys: pubkeys,
	}
	reqBytes, err := json.Marshal(req)
	if nil != err {
		return fmt.Errorf("fail to marshal req to json: %w", err)
	}
	fmt.Println(string(reqBytes))

	client := pester.New()
	client.MaxRetries = 15
	client.Timeout = 150 * time.Second
	client.KeepLog = true

	resp, err := client.Post(url, "application/json", bytes.NewBuffer(reqBytes))
	if nil != err {
		return fmt.Errorf("fail to send keygen request to(%s): %w", url, err)
	}
	defer func() {
		if err := resp.Body.Close(); nil != err {
			fmt.Printf("fail to close response body: %s", err.Error())
		}
	}()
	if resp.StatusCode != http.StatusOK {
		fmt.Printf("Client: %+v\n", client)
		result, _ := ioutil.ReadAll(resp.Body)
		return fmt.Errorf("unexpected response status code from key gen: %d: %s", resp.StatusCode, result)
	}
	result, err := ioutil.ReadAll(resp.Body)
	if nil != err {
		return fmt.Errorf("fail to read from response body: %w", err)
	}
	fmt.Println(string(result))
	return nil
}
