package main

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"time"

	bkeygen "github.com/binance-chain/tss-lib/ecdsa/keygen"
)

func main() {
	preParams, err := bkeygen.GeneratePreParams(time.Minute * 5)
	if err != nil {
		panic(err)
	}
	buf, err := json.Marshal(preParams)
	if err != nil {
		panic(err)
	}
	fmt.Println(hex.EncodeToString(buf))
}
